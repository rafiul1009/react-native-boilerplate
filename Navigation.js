import React from 'react';
import { Icon } from 'react-native-elements'

import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator, createTabNavigator } from 'react-navigation-tabs';

import InplayScreen from './screens/InplayScreen';
import AdvanceScreen from './screens/AdvanceScreen';
import ScoreScreen from './screens/ScoreScreen';

const InplayStack = createStackNavigator({
    Inplay: {
        screen: InplayScreen,
        navigationOptions: ({ navigation }) => {
            return {
                headerTitle: 'Inplay',
                headerLeft: (
                    <Icon
                        name='ios-menu'
                        type='ionicon'
                        size={30}
                        iconStyle={{ paddingLeft: 10 }}
                        onPress={() => navigation.openDrawer()}
                    />
                )
            }
        }
    }
});

const AdvanceStack = createStackNavigator({
    Advance: {
        screen: AdvanceScreen,
        navigationOptions: ({ navigation }) => {
            return {
                headerTitle: 'Advance',
                headerLeft: (
                    <Icon
                        name='ios-menu'
                        type='ionicon'
                        size={30}
                        iconStyle={{ paddingLeft: 10 }}
                        onPress={() => navigation.openDrawer()}
                    />
                )
            }
        }
    }
});

const ScoreStack = createStackNavigator({
    Score: {
        screen: ScoreScreen,
        navigationOptions: ({ navigation }) => {
            return {
                headerTitle: 'Score Updates',
                headerLeft: (
                    <Icon
                        name='ios-menu'
                        type='ionicon'
                        size={30}
                        iconStyle={{ paddingLeft: 10 }}
                        onPress={() => navigation.openDrawer()}
                    />
                )
            }
        }
    }
});

const AppTabNavigator = createBottomTabNavigator({
    Inplay: {
        screen: InplayStack,
        navigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ tintColor }) => (
                <Icon name='ios-tennisball' type='ionicon' size={25} color={tintColor} />
            )
        })
    },
    Advance: {
        screen: AdvanceStack,
        navigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ tintColor }) => (
                <Icon name='ios-rocket' type='ionicon' size={25} color={tintColor} />
            )
        })
    },
    Score: {
        screen: ScoreStack,
        navigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ tintColor }) => (
                <Icon name='ios-planet' type='ionicon' size={25} color={tintColor} />
            )
        })
    }
},{
    tabBarOptions: {
        activeTintColor: "#f4e849",
        inactiveTintColor: "white",
        style: {
            backgroundColor: "#094935",
            borderTopWidth: 0,
            shadowOffset: { width: 5, height: 3 },
            shadowColor: "black",
            shadowOpacity: 0.5,
            elevation: 5,
            paddingBottom: 5
        }
    },
    navigationOptions: ({ navigation }) => {
        const { routeName } = navigation.state.routes[navigation.state.index];

        return {
            header: null,
            headerTitle: routeName
        };
    }
});

const AppStackNavigator = createStackNavigator({
    AppTabNavigator: AppTabNavigator
}, {
    defaultNavigationOptions: ({ navigation }) => {
        return {
            headerLeft: (
                <Icon
                    name='ios-menu'
                    type='ionicon'
                    size={30}
                    iconStyle={{ paddingLeft: 10 }}
                    onPress={() => navigation.openDrawer()}
                />
            )
        };
    }
});

const AppDrawerNavigator = createDrawerNavigator({
    App: { screen: AppStackNavigator }
});

const AppSwitchNavigator = createSwitchNavigator({
    App: { screen: AppDrawerNavigator }
});

const AppContainer = createAppContainer(AppSwitchNavigator);

export default AppContainer;