import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import AppContainer from './Navigation';
import InplayScreen from './screens/InplayScreen'

const App = () => {
  return (
    <View style={styles.container}>
      <AppContainer />
      {/* <Text>Nice</Text> */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  }
});

export default App;
