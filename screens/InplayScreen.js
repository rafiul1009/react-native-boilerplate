import React, { Component } from 'react'
import { View, Text, StyleSheet, ScrollView } from 'react-native'


class InplayScreen extends Component {
    render() {
        return (
            <ScrollView>
                <View>
                    <Text style={styles.title}>Hellow Inplay</Text>
                </View>
            </ScrollView>            
        )
    }
}

const styles = StyleSheet.create({
    title: {
        fontFamily: "OpenSans-Light"
    }
});

export default InplayScreen
